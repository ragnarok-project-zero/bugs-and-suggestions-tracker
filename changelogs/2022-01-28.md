# Changelogs for Patch 1.6.3
Hello everyone,
we want to wish you all a Happy New Year! We're all really grateful that you are here with us enjoying the most authentic Zero experience.

While WoE is disabled, we hope that this patch introducing a revamped Battlegrounds system will scratch that PVP itch until we can go back to WoE for good.

We honestly believe that 2022 is going to be a fantastic year for Project Zero! We have a lot of great stuff planned and we are excited for you to try it out.
Make sure to join our Discord server to catch any previews.

Best,
Ema

# Battlegrounds
With this update, we would like to present to you our updated Battlegrounds system.
The new system has a dynamic queue that anybody can join from a town or from the Battlegrounds arena using the `@joinbg` command.

You can get BG exclusive supplies and consumables very cheap for your BG tokens, so you can keep that BG grind going for hours! We have also added a first round of BG rewards. We are looking to update these in the future, too.
A big thank you to our players Batcat, daddyoink, bahamut and LuBu for picking most of these costume suggestions!

For more details, please check out our wiki page: https://wiki.playragnarokzero.com/wiki/Battlegrounds.

We would also like to give a shout-out to the players that have been helping us test this new content on our test server Sakray!
This was a bit of an experiment, and we think it was a total success.
A special shout-out to Spirit, who has been very passionate and dedicated to make these new BGs a success.
We couldn't have done this without you!

# Quality of life
In 2022, we are hoping to expect a lot of new players and returning players to the server.
So we have been looking closely at all your suggestions that you have shared in Discord or on Gitlab.
In the coming updates, we are trying to introduce some QoL changes that will make it easier for new players to catch up.
Of course, some of these changes will also positively impact our existing player base.

## Random Job Essence enchants
As most of you probably know, you can add very powerful Job Essences to your Memorial Dungeon armor.
If this concept is new to you, we recommend you to check out the wiki on this topic: https://wiki.playragnarokzero.com/wiki/Memorial_Dungeon_Equipment.

We have heard your complaints that the large pool of possible Job Essence options, combined with the high RNG element, can be really frustrating.
Getting the desired Job Essence can be a very expensive journey if the Gods of Luck aren't in your favor.

To help remove this pain point, we are adding an option that lets you choose your Job Essence Lv2 for 15,000,000 Zeny and 20 Daily Coins.
Of course, other requirements like +9 refinement are still in place.

Thanks to Aymerhic for posting this suggestion on Gitlab: https://gitlab.com/ragnarok-project-zero/bugs-and-suggestions-tracker/-/issues/885.

## Increased Fever Field drops
The [Fever Fields](https://wiki.playragnarokzero.com/wiki/Fever_System) that we currently have are one source for Mythril Ore and Blacksmith Blessing Shards (BSB).
However, they vary greatly in terms of difficulty.
To make some of these maps more appealing for farming and to make Mythril and BSB more available, we have increased the drop chances for Mythril Ore and BSB for some of these:

- Orc Fever: BSB now drops at 0.05%
- Glastheim Fever:
  - BSB and Mythril Ore now drops at 0.09%
  - Blue Flame Essence now drops at 0.2%
- [Updated] Magma 3F:
  - Mythril Ore now drops at 0.15%
  - BSB now drops at 0.15%
- Odin 3F and Abyss Lake 4F:
  - Mythril Ore now drops at 0.4%
  - BSB now drops at 0.2%

Thank you to Vikai ([#892](https://gitlab.com/ragnarok-project-zero/bugs-and-suggestions-tracker/-/issues/892)) for the suggestion!

## Other changes
* Reduced the cost of Giant Fly Wing in the Coin Shop to 2 Daily Coins

# Cash Shop
The winter is a bit colder this year in Rune-Midgard than ever, let's get a bit cozy with new collection of warm and fluffy costumes!

* ![Bear Backpack](https://i.gyazo.com/a717fda3576a9285878850a428f0434d.gif)
* ![Lunatic Backpack](https://i.gyazo.com/2c5c72efa024f201e652d29dfcdc3e9a.gif)
* ![Shura Wild Long](https://i.gyazo.com/67e469b7e00ae269f9dab0bcc6841d48.gif)
* ![Released Ground](https://i.gyazo.com/e80ddf9ff3cb242b97b4aa7ce7135af5.gif)
* ![Bear Head](https://i.gyazo.com/1145b5335aec0e219825d313fa833ff7.gif)
* ![Fluffy Rabbit Cape](https://i.gyazo.com/0ac5d4f6b58c3007142cd23684ab2567.gif)

# Bug fixes
* Fixed AOE of Brandish Spear to originate from the user instead of the target ([#759](https://gitlab.com/ragnarok-project-zero/bugs-and-suggestions-tracker/-/issues/759))
* Removed unused warp to `hu_fild03` ([#889](https://gitlab.com/ragnarok-project-zero/bugs-and-suggestions-tracker/-/issues/889))
* Fixed Ninja's Shadow Slash to be considered melee ([#757](https://gitlab.com/ragnarok-project-zero/bugs-and-suggestions-tracker/-/issues/757))
