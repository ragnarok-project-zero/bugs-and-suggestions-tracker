# Changelogs for Patch 1.3.0

# Content Update: Amatsu
The next big content update is here, and we would like to invite all our players to [**Amatsu**](https://wiki.playragnarokzero.com/wiki/Amatsu), a beautiful city in the style of old Japan. With this update, players will also be able to choose a new job: the [Ninja class](https://wiki.playragnarokzero.com/wiki/Ninja). Please check out the corresponding pages on our Wiki to get all the information.

## Quests
Amatsu introduces 4 new quests, one of them is a new exclusive regional headgear creation, you can know more about these at [**Amatsu Quests**](https://wiki.playragnarokzero.com/wiki/Amatsu#Quests)

We also introduced two new headgears [General's Helmet](https://cp.playragnarokzero.com/item/view/?id=19362) and [Charcoal stove](https://cp.playragnarokzero.com/item/view/?id=19363) you can know more about how to create these at [**Amatsu headgear quests**](https://wiki.playragnarokzero.com/wiki/Headgear_Ingredients#Amatsu_Headgears)

## Localized Weapons: The Yin-yang Weapons
Outside of the quests, the other draw for players to visit Amatsu are the Yin-yang Weapons. These Yin-Yang Weapons are part of the Regional Weapons Category, which will be specific per global city as with Louyang's Earth Dragon Weapons, they also have the choice to reroll options and have their own set of option tables. you can know more about those at [**Souvenirs: The Yin-yang Weapons**](https://wiki.playragnarokzero.com/wiki/Amatsu#Souvenirs:_The_Yin-yang_Weapons)

## Dungeon: Tatami Maze, Battlefield in the Underground Forest, Underground Shrine
Deep in the heart of the Lakeside Castle lays a shadowy secret. Rumour has it that there is a hidden entrance that leads to a mysterious series of dungeons. The first of these is the Tatami Maze, a deceptive labyrinth of endless tatami and rows upon rows of rooms. None of what you see here is real, but many have been misled to believe otherwise... and have since been aimlessly walking into dead ends.

The second is the Battlefield in the Underground Forest, a cavern where many battles have taken place. And the last is the Haunted Shrine, also known as the Underground Shrine. There have been rumours (in hushed tones) of what lies in the Haunted Shrine, but no one has ever discovered what it is. All that is known is that the place has been sealed, and the ominous aura it exudes could ward off even the most courageous of warriors. The last level of the dungeon is where the MVP Boss Incantation Samurai or Samurai Specter resides.

You can know more about this at [**Amatsu Dungeon**](https://wiki.playragnarokzero.com/wiki/Amatsu_Dungeon).

## Syrups
Syrups are a new way to aid your experience in PvE, they offer much higher amounts of SP and HP restoration at a slightly higher prices and with a cooldown per use, these potions will ensure your Experience througout the game is much smoother, allowing for a more linear progression experience.

## Tool Dealer overhaul
The general tool dealers in all towns have been overhauled with a consistent inventory of items.
Every town now offers the same set of dealers:

* **Tool Dealers** for general items such as Fly Wings and Magnifiers
* **Potions Merchants** for your potions
* **Arrow Merchants** to buy arrows and convert them into quivers
* **Ninja Merchants** to buy Ninja ammo and convert them into boxes
* **Pouch Merchants** to convert gemstones into gemstone pouches
* **Advanced Potion Traders** to sell Syrup, a new type of strong potions

# Content Update: Ninja
Ninjas are finally here in Project Zero's world! In Zero Ninja comes with tons of improvements and changes to their skills. The following is a full listing of Ninja skill changes vs ordinary Ragnarok.


## Throw Shuriken
* Added a cooldown of 0.3s
* SP cost increased to 5
* Adjusted damage to (100 + 5 * SkillLevel)% ATK

## Throw Kunai
* Adjusted after-cast delay to 0.3s
* Added cooldown of 0.7s
* Changed SP cost to 10 for all levels
* Adjusted damage to (100 * SkillLevel)% ATK

## Throw Huuma Shuriken
* Adjusted variable cast time to 0.5s for all levels
* Adjusted fixed cast time to 0.5s
* Added cooldown of 2s
* Adjusted after-cast delay to 0.3s
* Changed SP cost to 15/20/25/30/35
* Adjusted damage to (250 * SkillLevel - 50)% ATK
* Now targets enemies instead of the ground
* Has increased splash from level 4

## Throw Zeny
* Adjusted after-cast delay to 0.3s
* Added cooldown of 5s

## Flip Tatami
* Adjusted after-cast delay to 0.3s
* Added cooldown of 2s

## Haze Slasher
* Adjusted after-cast delay to 0.3s
* Added cooldown of 1s
* Changed SP cost to 8 SP
* Adjusted damage to (100 + 20 * SkillLevel)% ATK

## Shadow Leap
* Adjusted after-cast delay to 0.3s
* Added cooldown of 1s

## Shadow Slash
* Adjusted after-cast delay to 0.3s
* Added cooldown of 1s
* Adjusted SP cost to 10/11/12/13/14
* Adjusted damage to (150 * SkillLevel - 50)% ATK
* Visible damage is split into 3 hits

## Cicada Skin Shed
* Adjusted after-cast delay to 0.3s
* Added cooldown of 5s

## Mirror Image
* Adjusted after-cast delay to 0.3s
* Added cooldown of 1s

## Flaming Petals
* Adjusted after-cast delay to 0.3s
* Added cooldown of 0.5s
* Adjusted fixed cast time to 0.5s
* Adjusted variable cast time to 1.05s/1.1s/1.15s/.../1.5s

## Blaze Shield
* Adjusted after-cast delay to 0.3s
* Added cooldown of 5s

## Exploding Dragon
* Adjusted after-cast delay to 0.3s
* Added cooldown of 1s
* Adjusted fixed cast time to 0.5s
* Adjusted variable cast time to 1.05s/1.1s/1.15s/.../1.5s
* Adjusted damage to 150 * (SkillLevel + 1)% MATK

## Freezing Spear
* Adjusted after-cast delay to 0.3s
* Added cooldown of 0.5s
* Adjusted fixed cast time to 0.5s
* Adjusted variable cast time to 1.05s/1.1s/1.15s/.../1.5s
* Adjusted damage to 150 * (SkillLevel + 1)% MATK

## Watery Evasion
* Adjusted after-cast delay to 0.3s
* Added cooldown of 2s

## Snow Flake Draft
* Adjusted after-cast delay to 0.3s
* Added cooldown of 1s
* Adjusted fixed cast time to 0.7s/0.6s/0.5s/0.4s/0.3s
* Adjusted variable cast time to 1.2s/1.1s/1s/0.9s/0.8s

## Wind Blade
* Adjusted after-cast delay to 0.3s
* Added cooldown of 0.5s
* Adjusted fixed cast time to 0.5s
* Adjusted variable cast time to alternate between 1.25s and 1.5s every 2 levels (1.5s at level 10)

## Lightning Jolt
* Adjusted after-cast delay to 0.3s
* Adjusted variable cast time to 1.7s
* Adjusted fixed cast time to 0.3s
* Adjusted damage to 100 * (SkillLevel + 1)% MATK

## First Wind
* Changed to display damage as 5 multi-hit damage
* Adjusted SP cost to 17/19/21/23/25
* Adjusted damage to 100 * (SkillLevel + 1)% MATK
* Adjusted variable cast time to 0.8s for all levels
* Adjusted fixed cast time to 0.3s
* Adjusted after-cast delay to 0.3s
* Changed splash to 3x3
* Changed range to 5/6/7/8/9 cells

## Killing Strike
* Adjusted variable cast time to 0.5s
* Adjusted fixed cast time to 1s
* Added cooldown of 5s

# Cash shop
In celebration of Ninja release we're releasing a limited time (Until first week of march) set of costumes inspired by Ninjas and Amatsu!

* ![Sakura Coronet](https://i.gyazo.com/ac2dcb52df75abb293343394c2a92711.gif)
* ![Black Musang Hat](https://i.gyazo.com/788ea7fdf4224615feadb39e9ef80100.gif)
* ![Krathong Crown](https://i.gyazo.com/c80335326223b39de17638c5565963f2.gif)
* ![Onihime Camellia](https://i.gyazo.com/075b7801dd15252031d04446077d1e66.gif)
* ![Katashiro Flying](https://i.gyazo.com/745c6437f8deebe61e1d3faa99916913.gif)
* ![Resonate Taego](https://i.gyazo.com/d0dd1714977552384ada07aa293ebd86.gif)
* ![Japanese Hair](https://i.gyazo.com/91dccca3937fc2665bdb9116bf2745b5.gif)
* ![Calabash](https://i.gyazo.com/c071d509424ad5ca68f8d4f75abeb2c2.gif)
* ![Sakura Festival Ribbon](https://i.gyazo.com/5f9753c94401d5bc5798e3a9e0ec31ed.gif)

# Fixes
* Removed Violin and Wire whip from OBB/OPB drop list, items has been removed from players as well.
* Fixed Witch's Broom description now it state it refine bonus correcctly.
* Fixed overweight icon to state 70% weight instead of 50% weight.
* Fixed wigs rendering, they shall no longer overwrite headgears.
