# Changelog for Patch 1.12.8

## Content updates
- Added new statues to commemorate the winners of the Race to 120! The owners can talk to their statue to receive the rewards

## Bug fixes
- Fixed the weight of Large WoE Violet Potion
- Sage's Endow skills can now correctly overwrite existing Endows of other elements
- Sage's Endow effects are now correctly lost upon death and can be dispelled
- Homunculus is now correctly affected by invincibility timers, just like their owners
