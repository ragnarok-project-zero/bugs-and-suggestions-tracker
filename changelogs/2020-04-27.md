**Disclaimer** During beta period some specific systems might be not working as intended, not working at all or abusable, for any abusable bugs found we will ensure all of it effect is reverted back and if necessary rollback characters.

**This update was focused on bringing new content**

# Fixes
* Novice combination kit (Obtainable at the novice academy) is now functioning as intended.
* Reverted a change from Hercules which caused various skill errors.
* Alchemist books are no longer discountable/overchargeable
* Swift Champions would no longer teleport
* Fixed Champions always dropping 2 optioned gears, they should instead drop 2 optioned gears with a slight chance for it to be 1 optioned
* Fixed Champion spawns in the following maps cmd_fild06, mjolnir_09, pay_fild04, prt_fild02
* Fixed Crazy Uproar having no cooldown now it correctly have 30 seconds cooldown
* Exp penalty for lower level monsters is restored

# Content
* Jerdis from prontera regional quests have new regional quests, help him getting corn and you may get a very nice buff.
* Champions would now despawn after 15 minutes from last attack, giving the chance to respawn them again.
* Skill and Status reset cooldown is now reduced to 12 hours, this is only effective during beta.

## Fever Fields
Fever fields are a much stronger version of monsters we already know and love, don't get fooled by their appearances trust us they do hurt. You can identify those fields by their red warp portals.

In fever fields monsters drops new items and even better options, normal monsters drops armors and weapons with up to 3 options, first 2 options is guaranteed while the third drops at random chance.

For boss monsters armors and weapons drops with up to 4 options, 3 of them is guaranteed while the 4th drops at random chance.

In this maintenance we've introduced those 3 fever fields, and there's still more to come.

* Implemented morocc fever field (moc_fild17)
	* Introduced the following new monsters
		* Desert Frilldora
		* Desert Sandman
		* Desert Hode
		* Desert Phreeoni
			* To spawn dessert Phreeoni you need to kill 10000 Desert Hode
	* Introduced the following new items
		* Desert Leather Armor
		* Desert Leather Robe
		* Desert Leather Suits
		* Desert Leather Boots
		* Desert Leather Sandal
		* Desert Leather Shoes

* Implemented Geffen fever field (gef_fild02)
	* Introduced the following new monsters
		* Dark Orc Warrior
		* Dark Orc Lady
		* Dark Orc Baby
		* Dark High Orc
		* Dark Orc Archer
		* Dark Orc Hero
			* To spawn Dark Orc Hero you need to kill 10000 Dark High Orc
		* Dark Orc Lord
			* To spawn Dark Orc Lord you need to kill 10000 Dark Orc Archer
	* Introduced the following new items
		* Orchish Dagger
		* Orchish Blade
		* Orchish Smasher
		* Orchish Mace
		* Orchish Staff
		* Orchish Cleaver
		* Orchish Bow

* Implemented payon fever field (pay_fild06)
	* Introduced the following new monster
		* Dark Willow
		* Dark Mandragora
		* Dark Stem Worm
		* Dark Spore
	* Introduced the following new items
		* Oak Armor
		* Oak Sword
		* Oak Stunner
		* Oak Mace
		* Oak Staff
		* Oak Leaf Dagger
		* Oak Leaf Robe
		* Oak Stem Bow
		* Oak Stem Suit

## Skill Balances

Zero has introduced a lot of skill balances to second job skills to fit better into the renewal environment, we have implemented all skill balance update till before Trans was released in kRO:Zero.

### Knight
#### ASPD
In addition to skill rebalances Knights will benefit of a reduction of Two handed sword penalty by 10, this mean you will roughly gain 10 more ASPD points when a Two handed sword is worn compared to before this patch.

#### Two Hand-Quicken
In addition to it original bonus Two Hand-Quicken now adds an additional 10% ASPD with its original ASPD buff.
It also adds:
	* 2 + 2 * skill level Crit
	* 2 * skill level hit points

#### Bowling Bash
Bowling bash base damage formula has stayed the same however additional bonuses and number of hits has been changed, first thing gutter lines is removed and replaced by a simple 5x5 AoE.

The skill now hits for 2 times, however if worn with two-handed sword and hitting 2 or more monsters you will hit 3 times and if 4 or more you will hit 4 times. Effectively making you hit at 2000% ATK with 4 or more targets.

Variable cast time was removed and Fixed cast time stays as it is at 0.35 seconds, in addition 2 seconds of Cooldown was added, and the amount of knockback tiles was changed to depend on the level.

| Level | Knockback Tiles |
| :---: | :-------------: |
| 1     | 1               |
| 2     | 1               |
| 3     | 2               |
| 4     | 2               |
| 5     | 3               |
| 6     | 3               |
| 7     | 4               |
| 8     | 4               |
| 9     | 5               |
| 10    | 5               |

#### Brandish Spear

The irregular aoe of Brandish Spear has been adjusted to a simple square in front of the caster, the following picture explains the different damage tiers and attack ratios.

![Brandish Spear Effect Range](.resources/brandish_spear_effect_range.png)

| Level | ATK (1) | ATK (2) | ATK (3) | ATK (4) | Attack Ranges |
| :---: | :-----: | :-----: | :-----: | :-----: | :-----------: |
| 1     | 120%    | -       | -       |  -      | 1             |
| 2     | 140%    | -       | -       |  -      | 1             |
| 3     | 160%    | -       | -       |  -      | 1             |
| 4     | 270%    | 180%    | -       |  -      | 1 ~ 2         |
| 5     | 300%    | 200%    | -       |  -      | 1 ~ 2         |
| 6     | 330%    | 220%    | -       |  -      | 1 ~ 2         |
| 7     | 420%    | 360%    | 240%    |  -      | 1 ~ 3         |
| 8     | 455%    | 390%    | 260%    |  -      | 1 ~ 3         |
| 9     | 490%    | 420%    | 280%    |  -      | 1 ~ 3         |
| 10    | 562%    | 525%    | 450%    |  300%   | 1 ~ 4         |

SP amount has also been doubled up to 24

### Blacksmith
#### ASPD
In addition to skill rebalances Blacksmith will benefit of a reduction of Two handed axe penalty by 3, this mean you will roughly gain 3 more ASPD points when a Two handed axe is worn compared to before this patch.

#### Adrenaline Rush
In addition to the original bonus an additional 10% ASPD bonus was added, also the buff now gives 5 + 3 * skill level hit

#### Over thrust
The skill weapon breaking chance was removed, in addition the ATK% buff to other members of the party was changed

| Level | ATK% (Self) | ATK% (Other) |
| :---: | :---------: | :----------: |
| 1     | 5           | 5            |
| 2     | 10          | 5            |
| 3     | 15          | 10           |
| 4     | 20          | 10           |
| 5     | 25          | 20           |

#### Forging
Blacksmiths received a very powerful buff to their forging ability, on any level 3 or higher weapon they forge will get random options and the amount of your options depends on your ranking

| Rank       | Amount of options |
| :--------: | :---------------: |
| Non-Ranked | 3                 |
| 10th ~ 6th | 4                 |
| 5th ~ 1st  | 5                 |

### Priest
#### ASPD
In addition to skill rebalances Priests will benefit of a reduction of shield penalty by 2, this mean you will roughly gain 2 more ASPD points when a shield is worn compared to before this patch.

#### Suffragium
The limit of using it only for 1 cast is removed and now it can be used for as long as it duration, it now also casts on whole party including the priest casting it too.

The global down is reduced to 0.3 seconds, 1 second casting time is added, 0.3 seconds fixed cast time is added, 60 seconds cooldown is added and buff duration is increased to 60 seconds.

#### Impositio Manus
Now it casts on whole party including the priest casting it too, it also adds MATK beside its original ATK bonus it's also no longer a mystery bonus and will show in your status window.

The global down is reduced to 0.3 seconds, 1 second casting time is added, 0.3 seconds fixed cast time is added, 120 seconds cooldown is added and buff duration is increased to 120 seconds.

#### Magnus Exorcismus
This skill has been totally revamped to give priests more chance in being more involved in PvM, the skill now attacks all type of monsters not just Undead and Demon, also Undead and Demon will receive an extra 30% MATK damage using this skill now.

The cast time is reduced to 3 seconds, fixed cast time is reduced to 1 second, global cooldown reduced to 0.3 seconds and ground unit duration was reduced by 1 second for all levels.

#### Mace mystery
Beside its original bonus it now also adds 1 crit per level, please note it's a mystery bonus therefore it won't show up in your status window.

### Crusader
#### ASPD
In addition to skill rebalances Crusaders will benefit of a reduction of Two handed spear penalty by 2, this mean you will roughly gain 2 more ASPD points when a Two handed spear is worn compared to before this patch.

#### Spear Quicken
In addition of its original bonus an additional 10% ASPD was added.

#### Grand Cross
The feedback damage on inflicted on the caster was removed now you will only spend 20% of your remaining HP for each cast.

Cast time is reduced to 1 second, Fixed cast time is reduced to 0.5 seconds, Global cooldown is reduced to 0.3 seconds and 1 second of cooldown was added.

#### Shield Boomerang
The skill formula was changed to ((80 * skill level) + Shield Weight + (Shield Refine * 4))% ATK

A cooldown of 0.5 seconds is added and global cooldown reduced to 0.3 seconds.

### Monk
#### Size modifiers
Knuckle size modifiers was changed to Small 100% / Medium 100% / Large 75%

#### ASPD
In addition to skill rebalances Monks will benefit of a reduction of shield penalty by 2, this mean you will roughly gain 2 more ASPD points when a shield is worn compared to before this patch.

#### Chain Combo
Skill ATK% ratio was changed to 250 + 50 * skill level ATK% in addition knuckles now hits with double the damage and hits for 6 times.

The SP consumption was also lowered to the following table

| Level | SP  |
| :---: | :-: |
| 1     | 5   |
| 2     | 6   |
| 3     | 7   |
| 4     | 8   |
| 5     | 9   |

#### Combo Finish
Skill ATK% ratio was changed to 450 + 150 * skill level ATK%

The SP consumption was also lowered to the following table

| Level | SP  |
| :---: | :-: |
| 1     | 3   |
| 2     | 4   |
| 3     | 5   |
| 4     | 6   |
| 5     | 7   |

#### Blade stop (Root)
The skill duration was set to 10 seconds regardless of the skill level and boss type monsters can now be affected but only for 2 seconds.

The global cooldown is lowered to 0.3 seconds and a cooldown of 2 seconds is added.

#### Finger offensive
The skill now will hit for 5 times regardless of it skill level, the ATK ratio was modified to 600 + 200 * skill level ATK% and casting it on rooted enemy will increase it damage by 50%, the skill would also consume 1 Spirit sphere regardless of it level.

Reduced global cooldown to 0.3 seconds and 1 second cooldown was added.

#### Investigate
Changed damage ATK ratio to 100 * skill level and using it on rooted enemy will increase it damage by 50%.

Reduced global cooldown to 0.3 seconds.

#### Asura Strike
Asura strike would only require having 2 spirit spheres when used in combo.

SP Restoration debuff duration is reduced to 3 seconds.

### Assassin
#### Sonic Blow
The damage attack was adjusted to 200 + 100 * skill level ATK% it also deals 50% more damage for enemies with 50% or less HP.

A cooldown of 1 second was added.

**NOTE:** The skill is supposed to have it animation delay removed however this requires a newer client therefore it's not yet possible, we're going to have this update ASAP.

#### Venom Splasher
Adjusted damage formula to 400 + 100 * skill level ATK%, removed the Red Gemstone requirement.

In addition to this the cooldown and bomb delay was changed to the following.

| Level | Cooldown | Bomb cooldown |
| :---: | :------: | :-----------: |
| 1     | 12       | 11            |
| 2     | 11       | 10            |
| 3     | 10       | 9             |
| 4     | 9        | 8             |
| 5     | 8        | 7             |
| 6     | 7        | 6             |
| 7     | 6        | 5             |
| 8     | 5        | 4             |
| 9     | 4        | 3             |
| 10    | 3        | 2             |

In addition Poison React doesn't affect the bomb damage any longer.

### Rogue
#### ASPD
In addition to skill rebalances Rogues will benefit of a reduction of shield penalty by 2, this mean you will roughly gain 2 more ASPD points when a shield is worn compared to before this patch.

#### Intimidate
Learning intimate increases your ASPD by 1% for each level learned.

#### Slightless Mind
The damage formula was changed to 100 + 150 * skill level ATK%, the debuff hit limit was removed and the debug damage increase changed to 30% on normal targets and 15% on boss.

The SP cost is lowered to 15.

#### Backstab
The damage formula was adjusted to 300 + 40 * skill level ATK%, it also hits twice if you're using a dagger.

The skill would also teleport you behind your enemy and can no longer be used while hiding, the skill also no longer ignores flee however on every level you learn 1 hit will be added to the hit calculation.

SP consumption is reduced to 16.

### Mage
#### Fire Bolt, Cold Bolt, Lightning Bolt and Napalm Beat.

Cooldown is adjusted to 0.7 seconds and global cooldown is adjusted to 0.3 seconds.

#### Thunder storm
Cooldown of 1.7 seconds is added and global cooldown is reduced to 0.3 seconds.

#### Soul Strike
Cooldown of 0.9 seconds is added and global cooldown is reduced to 0.3 seconds.

#### Frost Drive
Cooldown of 1.2 seconds is added and global cooldown is reduced to 0.3 seconds.

#### Fire Ball
Cooldown of 1.2 seconds is added and global cooldown is reduced to 0.3 seconds.

### Wizard
#### Meteor Storm
Lowered cast time to 7.5 seconds, lowered global cooldown to 0.3 seconds and reduced fixed cast time to 1.2 seconds.

Cooldown was also adjusted based on level.

| Level | Cooldown |
| :---: | :------: |
| 1     | 1.7      |
| 2     | 2.7      |
| 3     | 2.7      |
| 4     | 3.7      |
| 5     | 3.7      |
| 6     | 4.7      |
| 7     | 4.7      |
| 8     | 5.7      |
| 9     | 5.7      |
| 10    | 6.7      |

#### Lord of Vermilion
The number of hits was increased to 20 hits, global cooldown is decreased to 0.3 seconds, blind duration is changed to 30 seconds * skill level and chance to 10 + 5 * skill level%

Cooldown, Fixed cast time is also changed based on level.

| Level | Cooldown | Fixed cast time |
| :---: | :------: | :-------------: |
| 1     | 6        | 1.52            |
| 2     | 5.9      | 1.44            |
| 3     | 5.8      | 1.36            |
| 4     | 5.7      | 1.28            |
| 5     | 5.6      | 1.2             |
| 6     | 5.5      | 1.12            |
| 7     | 5.4      | 1.04            |
| 8     | 5.3      | 0.96            |
| 9     | 5.2      | 0.88            |
| 10    | 5.2      | 0.8             |

#### Storm Gust
The damage formula was changed to 70 + 50 * skill level MATK%, Cooldown of 4.7 seconds is added. Also the pre-renewal behavior of the skill is restored.

Cast time and fixed cast time is also changed based on level.

| Level | Cast time | Fixed cast time |
| :---: | :-------: | :-------------: |
| 1     | 5.1       | 0.75            |
| 2     | 5.2       | 0.80            |
| 3     | 5.3       | 0.85            |
| 4     | 5.4       | 0.90            |
| 5     | 5.5       | 0.95            |
| 6     | 5.6       | 1               |
| 7     | 5.7       | 1.05            |
| 8     | 5.8       | 1.10            |
| 9     | 5.9       | 1.15            |
| 10    | 6         | 1.20            |

#### Jupitel Thunder
Decreased fixed cast time to 0.5 seconds.

Cast time is also changed based on level.

| Level | Cast time |
| :---: | :-------: |
| 1     | 2         |
| 2     | 2.2       |
| 3     | 2.4       |
| 4     | 2.6       |
| 5     | 2.8       |
| 6     | 3         |
| 7     | 3.2       |
| 8     | 3.4       |
| 9     | 3.6       |
| 10    | 3.8       |

#### Earth Spike
The damage formula was changed to 200% MATK, a cooldown of 0.7 seconds is added and global cooldown reduced to 0.3 seconds

#### Quagmire
Added cooldown of 0.7 seconds and reduced global cooldown to 0.3 seconds

#### Heaven's Drive
Added cooldown of 0.7 seconds and reduced global cooldown to 0.3 seconds

#### Fire Pillar
Increased the splash range from level 6 and onward to 3 cells, the requirement of Blue gemstone is added again, added cooldown of 0.7 seconds and reduced global cooldown to 0.3 seconds.

### Sage
#### ASPD
In addition to skill rebalances Sage will benefit of a reduction of shield penalty by 2, this mean you will roughly gain 2 more ASPD points when a shield is worn compared to before this patch.

#### Endow Blaze, Endow Quake, Endow Tsunami and Endow Tornado
Catalysts was changed to the Point items which can be purchased from Geffen City and Geffen Tower, the skill can no longer fail and it now increases magic attacks of the same element as the endow.

Fixed cast time is also reduced to 1 second and 1 second of cast time is added.

The skill duration time has been edited as the following

| Level | Duration in seconds |
| :---: | :-----------------: |
| 1     | 600                 |
| 2     | 900                 |
| 3     | 1200                |
| 4     | 1500                |
| 5     | 1800                |
| 6     | 1800                |
| 7     | 1800                |
| 8     | 1800                |
| 9     | 1800                |
| 10    | 1800                |

#### Volcano, Whirlwind and Deluge
Lowered cast time to 3 seconds, fixed cast time to 0.5 seconds and changed catalyst to Blue Gemstone

#### Land Protect
Lowered cast time to 3 seconds and fixed cast time to 0.5 seconds.

The duration was also changed based on level

| Level | Duration in seconds |
| :---: | :-----------------: |
| 1     | 120                 |
| 2     | 165                 |
| 3     | 210                 |
| 4     | 255                 |
| 5     | 300                 |

#### Magic Rod
Added cooldown of 0.7 seconds and global cooldown is adjusted to 0.3 seconds.

#### Auto spell

The skill trigger list was adjusted new skills can be used the full list is:
Fire bolt / Cold bolt / Lightning bolt / Soul strike / Fire ball / Frost diver / Earth spike / Thunder storm / Heaven's drive

The level of the triggered skill is no longer random and it increases
Depending on the skill level you learned / 2 with maximum of level 5


Adjusted auto spell trigger chance to 2 * auto spell skill level

**NOTE:** The current client cannot handle more than 7 skills in the auto-spell list, due to this some skills just

#### Volcano
It now also increases the MATK of targets inside.
Adjusted increase formula to 5 + 5 * skill level.

### Hunter
#### ASPD
In addition to skill rebalances Hunters will benefit of a reduction of bow penalty by 1, this mean you will roughly gain 1 more ASPD points when a bow is worn compared to before this patch.

#### Land Mine, Blast Mine, Claymore Trap
Fixed cast time is removed and cast time changed to 0.5 seconds.

#### Blitz Beat
The damage no longer splits over enemies and it scales on AGI and DEX

### Alchemist
#### Demonstration
The damage formula was slightly buffed up, it also receives a bonus from Learning Potion skill depending on it level

Weapon breaking chance has been changed to 3 * skill level%

The skill also recieved a new damage formula

#### Acid Terror
Now it shows damage as 5 hits and damage formula was changed to 200 * skill level ATK%, it also receives a bonus from Learning Potion skill depending on it level.

Armor destruction chance has been changed to 5 * skill level

The skill also received a new damage formula
