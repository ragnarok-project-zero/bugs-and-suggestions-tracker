# Changelogs for Patch 1.5.5

This is a hot patch bringing QoL changes.

# Event
## Halloween
Yvain is a warrior who was consumed by evil forces, help him by defeating the evil force within him.

This event is a new worldboss which will be available throughout November, afterwards both worldbosses will come at random, the new world boss consists of 4 phases where the 4th is an optional phase and spawning a separate treasure box.

The first treasure box will be dropping the regular WB box, while the optional boss will drop Halloween exclusive items consists of 2020 Halloween items and a new pet Scatleton, the box also drops Halloween coins at a chance that can be exchanged at Sakray in niflheim.

This event also brings a new special mechanic, we're not going to spoil it but all we can say is be wary of where you look.

* The new Scatleton pet
	* ![Scatleton](https://i.gyazo.com/48728a06759e2fa81fc086be50fe92ce.gif)

# QoL
* Soul link scroll has been updated, now it's sold at 750,000 zeny and unlocks all 3 levels of Twilight Alchemy.

# Cash shop
We're releasing 4 new costumes this Halloween beside 2020 Halloween costumes!

* Niflheim Key
	* ![Niflheim Key](https://i.gyazo.com/a76f9389a74bdce8fb09a8bd0268a555.gif)
* Inner color long hair
	* ![Inner color long hair](https://i.gyazo.com/ead4b14e331cefbefac33b5bc1aa5e50.gif)
* Astrologer Hat
	* ![Atrologer Hat](https://i.gyazo.com/147fa487369a344486c2eac16a7c280b.gif)
* Avenger
	* ![Avenger](https://i.gyazo.com/90f6937e342f11d34b15d7059659afef.gif)
