# Changelog for Patch 1.13.0

Hello Project Zero folks!

The Restart Event is now officially over and we are merging the Restart server with the main server.

We want to thank everybody that participated in the Restart event. It has been really refreshing and fun to see what y'all made with this time. We also hope that this has sparked interest in the game again for some of you.

The participation has definitely made us more motivated again to create new content for you, so stay tuned for more updates in the future!

See you ingame,
Project Zero

## End of Restart
With the end of Restart, we're making a few changes and removing content that was exclusive to Restart.
However, some of the content that was introduced during Restart is here to stay.

Here's what has been **removed**:
- EXP Weekends are gone
- The Base Level cut-off for Emily is back to Level 50
- The stat/skill resets are back to their regular costs
- Restart Eggs no longer drop

Here's what will **stay**:
- Memorial Dungeon resets per Master Account
- The increased Instance rewards are staying, except for Odin's Temple which goes back to pre-Restart rewards
- The reduced costs for Decarder will stay
- Every Master Account has had their Booster Pack count refreshed and is now eligible to pick up **one** permanent Booster Pack

### Important: Server merge
The previous state of the main server has been merged with the Restart server content.

**After logging in for the first time after Restart, we highly suggest that you check that everything is in place before you start jumping back into farming. So please make sure that your inventories, storages, guilds, etc. are still there for your Restart and pre-Restart characters.**

A few more things to note about the migration:

- The Zeny in your Bank has been merged from both servers
- All pre-Restart characters have had their skills reset due to the Skill Rebalance patch
- Your Guild's Emblem might need to be re-uploaded
- All your game accounts and their characters should be available now
  - If you created a character during Restart on an account and slot that was occupied by another character, the Restart character was moved to new account
  - This new account has the same name as the old account, plus a "_re" suffix
- The Master Storage contents of your Restart account have been moved to the **character storage** of a random character
  - The contents were moved to the storage of the first character that was found with an empy character storage
  - If you didn't have any free characters, a dummy character (name starting with "Storage") has been created for you
  - The easiest way to find the character that received the items is to use the Control Panel
  - If you're having problems finding the character, you can DM Timo on Discord with your Master Account email


## Sarah's Memory Instance
With this update, we are also releasing the next part of the roadmap: Sarah's Memory Instance. This is a new instance that can be accessed from the Dimensional Gap for people Level 100+. Talk to Explorer Tiger (`/navi dali 130/107`) to learn more.

Please note that the Geffen Night Arena part of this update is not released yet. It was not ready yet and we decided to not delay release Sarah's Memory any further. Instead we will release Geffen Night Arena in a few weeks, which hopefully also gives people a bit more time to play around with Sarah's Memory MD.

Our Wiki volunteers are still busy updating the Wiki page, so for now we encourage you to simply explore the content yourself and discover it on your own.

Here are some of the main parts of this content patch:

- You can visit the Sarah's Memory instance to collect Dimension Stones and Dimension Craft Stones
- You can exchange those rewards for different armor sets and accessories at the vending machine in the Dimensional Gap
- You can apply Random Options to Memorial Soldier Armor and High Elder Robe
- Talk to Designer Sally to enchant your Dimensional Gap armor
  - The third enchantment can be a Second Job or Trans Class Job Essence
  - There is no Perfect Enchant, but existing Job Essences can be upgraded to a stronger version of that job family
  - Target armor: [Forged Dimension Mail](https://cp.playragnarokzero.com/item/view/?id=450136), [Time Forged Mail](https://cp.playragnarokzero.com/item/view/?id=450137), [Runeforged Mail](https://cp.playragnarokzero.com/item/view/?id=450138), [Rift Forged Mail](https://cp.playragnarokzero.com/item/view/?id=450139)
