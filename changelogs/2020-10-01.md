# Changelogs for Patch 1.1.1

This maintenance was mainly focused on delivering new content and various QoL improvements.

This maintenance marks the final content update for 1.1.0 release, this said we're expecting a new map coming really soon before the next major update Louyang.

# Event
## Oktoberfest is here!
Every year by the end of September and until mid October a folk festival is held to celeberate; long time ago a king declared to have a nice festival for everyone to celeberate his marriage and enjoy tons of Weißbier.

In this event, help rescue the supplies of Weißbier so the festival doesn't get ruined for everyone and try your luck to get a lot of event-exclusive costumes and some real special and precious items.

Also help the Prontera monks defend their lands from the random Pasana attack and reward whole server with a special buff!

The event quest can be started by talking to Pub Manager Horst at `/navi prt_in 166/10` and you can make the special Oktoberfest Pretzels by talking to Elfriede at `/navi prontera 105/83`.

The special Oktoberfest Pretzels when eaten can give:
* Common
	* 5x Strawberry
	* 5x Fresh Fish
	* 1x Awakening Potion
	* 1x Berserk Potion
* Uncommon
	* 1x World Tour Ticket
	* 1x [Event] Rapid Potion
	* 1x [Event] Challenge Drink
	* 1x [Event] Unlimited Drink
	* 1x [Event] Power Drink
	* 1x [Event] Mimir Spring Water
	* 1x Costume Rain Cloud
	* 1x Costume Autumn Headband
	* 1x Costume Beer Cap
* Rare
	* 1x Costume Brown Stole
	* 1x Costume Tone of Gold
* Epic
	* 1x Costume Warm Cat Muffler
	* 1x Costume Large Ribbon Muffler
	* 1x Enriched Elunium
	* 1x Enriched Oridecon
	* 1x Blacksmith's Blessing Shard
* Heroic
	* 1x Costume Poring Bag (Garment Costume)
	* Blacksmith's Blessing

# Content
## New items
The following new drops have been added to the following monsters
* [Frozen Rose](https://cp.playragnarokzero.com/item/view/?id=19376) from Freezer
* [Loli Ruri Shoes](https://cp.playragnarokzero.com/item/view/?id=22194) from Loli Ruri
* [Owl Duke Stick](https://cp.playragnarokzero.com/item/view/?id=26148) from Owl Duke
* [Jewel Shield](https://cp.playragnarokzero.com/item/view/?id=28949) from Assaulter
* [Dullahan Glove](https://cp.playragnarokzero.com/item/view/?id=32201) from Dullahan
* [Droopy Turtle Hat](https://cp.playragnarokzero.com/item/view/?id=18668) from Permeter
* [Moon Rabbit Hat](https://cp.playragnarokzero.com/item/view/?id=5457) from Spring Rabbit

## Atelier Manus
Atelier Manus has just released their latest fashion trends, [Black Witch Hat](https://cp.playragnarokzero.com/item/view/?id=19358) is now available for crafting from Atelier Manus.

## Clock Tower
This patch introduces new bosses in Clock Tower, alongside a few drop changes and new drops.

### Fever Bosses
* kill 300x Arclouse to spawn 1x [Zieglouse](https://cp.playragnarokzero.com/monster/view/?id=20179) at `alde_dun01`
* kill 300x High Orc to spawn 1x [General Orc](https://cp.playragnarokzero.com/monster/view/?id=20178) at `alde_dun02`
* kill 300x Penomena to spawn 1x [Jennifer](https://cp.playragnarokzero.com/monster/view/?id=20177) at `alde_dun03`
* kill 1000x Bathory to spawn 1x [Erzsebet](https://cp.playragnarokzero.com/monster/view/?id=20176) at `alde_dun04`
* kill 100x Joker to spawn 1x [Extra Joker](https://cp.playragnarokzero.com/monster/view/?id=20175) at `alde_dun04`

### Hats
Two new hats are now available to be crafted from Clock Tower drops. They can be crafted using `Hat man` NPC at `/navi aldebaran 130/140`

* [Clock Tower Manager Headset](https://cp.playragnarokzero.com/item/view/?id=19333)
* [Pocket Watch Hair Ornament](https://cp.playragnarokzero.com/item/view/?id=19334)

***Note: We have greatly reduced the amount of items required for those items as well as adjusted the drop rates - in kRO it would require roughly 250,000 monster kills or using Gelstars (Cash Shop). We have reduced it to be roughly 80% less grindy on average***

### Quests
This update introduces 4 new regional quests for level 60 ~ 70 in Aldebaran, those quests can be taken from Serdin `/navi aldebaran 143/132`

## Glastheim
This update introduces new Fever Maps, new equipment, Regional Quests and a very competitive new Fever System!

### New Items
* [Khalitzburg Knight Armor](https://cp.playragnarokzero.com/item/view/?id=15281) from Khalitzburg
* [Wizard's Glove](https://cp.playragnarokzero.com/item/view/?id=32200) from Sting
* [Sting Hat](https://cp.playragnarokzero.com/item/view/?id=5509) from Sting
* [Royal Knight's Broadsword](https://cp.playragnarokzero.com/item/view/?id=21039) from Raydric
* [Royal Knight's Lance](https://cp.playragnarokzero.com/item/view/?id=26016) from Raydric
* [Zealotus Mask](https://cp.playragnarokzero.com/item/view/?id=5121) from Zealotus
* [Guardian Sword](https://cp.playragnarokzero.com/item/view/?id=13487) from Rybio
* [Thief Handcuffs](https://cp.playragnarokzero.com/item/view/?id=2913) from Phendark, Injustice
* [Prison Watcher](https://cp.playragnarokzero.com/item/view/?id=28032) from Injustice
* [Witch's Broom](https://cp.playragnarokzero.com/item/view/?id=26140) from Alice
* [Prisoner's Diary](https://cp.playragnarokzero.com/item/view/?id=28619) from Prison Breaker

### Return of bosses
Zealotus and Bloody Knight have returned to Glast Heim! They're now considered MVPs with new items and a little surprise!

### Fever Dungeons
#### Glast Heim Stairs
This new Fever Map offers a very new and competitive system along with one of the best staffs for Wizards! Two factions reign this Fever Map: Flame Ghosts and Ice Ghosts. When the kill count in either faction reaches 1,000, the faction-specific boss will spawn randomly on the map. Be aware as kills in either faction will impact the opposing faction's kill count!

Depending on the winning faction one of two bosses will spawn:
* [Awakened Flame Ghost](https://cp.playragnarokzero.com/monster/view/?id=20168)
* [Awakened Ice Ghost](https://cp.playragnarokzero.com/monster/view/?id=20169)

This dungeon drops the new Glacial Staff and Hellfire staff, those can be combined together to produce Blue Flame Staff
* [Hellfire staff](https://cp.playragnarokzero.com/item/view/?id=26138)
* [Glacial Staff](https://cp.playragnarokzero.com/item/view/?id=26139)
* [Blue Flame Staff](https://cp.playragnarokzero.com/item/view/?id=2049)

The combination requires various items from the new dungeon. The staff can be combined by talking to Altos `/naiv gl_step 110/127`. To make it even sweeter, the staff be enchanted by talking to Sarah `gl_step 177/133`

#### The Lowest Cave F1
Monster density has been increased and two new monsters have been added: [Contaminated Gargoyle](https://cp.playragnarokzero.com/monster/view/?id=20170) and [Contaminated Sting](https://cp.playragnarokzero.com/monster/view/?id=20171)

#### Glast Heim Chivlary F2
New monsters have been added: [Contaminated Raydric](https://cp.playragnarokzero.com/monster/view/?id=20172) and [Contaminated Raydric Archer](https://cp.playragnarokzero.com/monster/view/?id=20173)

#### Glast Heim Underprison F2
New monster has been added: [Prison Breaker](https://cp.playragnarokzero.com/monster/view/?id=20174)

### Regional quests
Glast Heim comes with new Regional Quests suited for end-game players level 80+. Their experience rewards are pretty high and suitable as an alternative leveling method.

* Darques at `/navi glast_01 213/294`
* Charon at `/navi glast_01 206/291`
* Nadir the wizard at `/navi glast_01 204/291`
* Claude at `/navi glast_01 205/131`
* Chiab at `/navi glast_01 67/193`

# Changes
* Added a Kafra to Eden which features storage access and teleport to all cities in Midgard
* Added  commodities to all Tool Dealers (most notable: Green Potion, Beserk Potion, Fire Arrows, Ygg Leaves ...)
* `@showrare` now supports decimal values (e.g. @showrare 0.2)
* City-based Kafra warps from city A -> city B will also support city B -> city A (round-trip)
* Added Al De Baran + Comodo Pharos Beacon as Warp Locations from Prontera
* Changed #ally color to purple
* The new Boy's Cap should be tradeable now
* Added the following items to buying store
	* Buff items you receive from regional quests
	* Jello Shards and Stones
	* Mithril and other Zero ingredients

# Fixes
* Fixed Conqueror's +27 Set Bonus to include Accessory refine bonuses (it only counted Armor/Garment/Shoes before)
* Fixed Chaos Lance recipe cost (2 -> 1 Chaos Jello Stone)
* Fixed Ant Hell [E]'s guaranteed loot range for Azure Crystals from 1-2 to 1-8 to be consistent with Izlude's drop range
* Adjusted guaranteed loot drop % chances to be consistent across all MDs (see wiki for rates)
* Corrected HP300 and HP400 enchants to be only available on Subjugation Armor +9 and higher only
* Fixed Monk essence casting Investiage on self instead of Call Spirit
* Fixed Boss monsters dropping physical options on magic weapons
* Fixed Rogue essence, hit bonus now uses Gank level for its bonus

# Skill fixes
* Changed Devotion Level Range from 10 -> 15 to align with party share range
* Changed Flasher Trap's blinding chance: (100% chance minus target resist)
* Changed Freezing Trap's freezing chance (100% chance minus target resist)
* Changed Frost Nova's splash from 2x2 to 5x5
* Changed Detect's range from 9->8 at max level and detection splash from 3x3 to 7x7 
* Changed Jupitel Thunder's knockback from 7 cells -> 12 cells at max level
* Changed Backsliding cost from 7SP -> 10SP
* Changed Impositio Manus SP cost from (13/16/19/22/25) to (59/62/65/68/71) [lv1-5 respectively]
* Changed Suffragium SP cost from 8 SP -> (45/57/69) [lv1-3 respectively]
* Changed Suffragium VCT% bonus from (15/30/45)% to (10/15/20)% [lv1-3 respectively]
