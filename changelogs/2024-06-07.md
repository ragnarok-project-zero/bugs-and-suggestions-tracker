# Changelog for Patch 1.11.2

# Event
It's already a few days into June, and you might know what that means.
It is time to celebrate our differences with Pride Month 2024!

Starting today, all monsters have a chance to drop a new Project Zero Event Egg!
We hope you like rainbows! It can contain the following items:

* Common
  * 5x Strawberry
  * 5x Fresh Fish
  * 1x Awakening Potion
  * 1x Berserk Potion
* Uncommon
  * 1x World Tour Ticket
  * 1x [Event] High Quality Meal
  * 1x [Event] Challenge Drink
  * 1x [Event] Unlimited Drink
  * 1x [Event] Power Drink
  * 1x [Event] Mimir Spring Water
  * 1x Costume: Rainbow Feathers
  * 1x Costume: Rainbow Popcorn
  * 1x Costume: Bouquet Valetta
* Rare
  * 1x Costume: Rainbow Veil
  * 1x Costume: Vassalage Necklace
* Epic
  * 1x Costume: Birthday Balloons
  * 1x Costume: Consecrate Fidis Oriora
  * 1x Enriched Elunium
  * 1x Enriched Oridecon
  * 1x Blacksmith's Blessing Shard
* Heroic
  * 1x Costume: Mystery Wings
  * Blacksmith's Blessing

# Content updates

## Bug fixes
* Fixed the Heal Amount enchantment for Temporal Boots (see https://gitlab.com/ragnarok-project-zero/bugs-and-suggestions-tracker/-/issues/1004)
* Fixed the ingame name for the Macaron Bunny Hat
