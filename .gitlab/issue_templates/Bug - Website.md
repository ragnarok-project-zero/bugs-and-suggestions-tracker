<!-- This template is meant for website issues like failing to register, reset password, forum issues and so on.
if the reported issue is exploitable and dangerous please check "This issue is confidential and should only be visible to team members with at least Reporter access." option underneath the issue description box, failing to do so will result in punishments to your account -->

## Summary

<!-- Add a brief description of the bug (between two to three lines) under this line -->

## Steps to reproduce

1.
2.
3. (Add more points as needed)

## Actual results

<!-- Add a description of what happens after the steps described above under this line -->

## Expected results

<!-- Add a description of what should happen instead under this line -->

## Additional information

<!-- Add any additional information if applicable under this line -->

### Computer details

- Operating System and Version:
- Browser and version:
- Country of connection:

### Notes

<!-- Any extra notes? feel free to share it under this line -->
